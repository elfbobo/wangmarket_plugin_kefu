<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.shiro.ShiroFunc"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../iw/common/head.jsp">
	<jsp:param name="title" value="选择模版"/>
</jsp:include>
<script src="plugin/kefu/template/${kefu.templateName}/config.js"></script>

<style>
#templateList{
	width:100%;
}
#templateList .item{
	float:left;
	text-align: center;
	padding: 20px;
}

#templateList .item .imgdiv img{
	width: 350px;
    border: none;
    max-height: 100%;
    max-width: 70%;
    padding-bottom: 12px;
}
#templateList .item .info{
	width: 100%;
    padding: 8px;
    font-size:12px;
    color:#c2bcbc;
}
</style>
<div id="templateList">
	<div class="item">
		<div class="imgdiv"><img src="{imgsrc}"></div>
		<div class="info">{info}</div>
		<div class="btn">
			<div class="layui-btn-group">
				<a class="layui-btn" href="/plugin/kefu/template/{templateName}/demo.html" target="_black">在线预览</a>
				<button class="layui-btn {useClass}" onclick="updateTemplate('{templateName}');">{shiyongbutton}</button>
			</div>
		</div>
	</div>
</div>




<script type="text/javascript">
//加载云端的客服模版
function loadTemplate(){
	var template = document.getElementById('templateList').innerHTML;	//item模版
	var html = '';	//绘制的html列表
	
	parent.iw.loading('加载中...');
	$.getJSON("/plugin/kefu/templateList.do", function(data){
	    parent.iw.loadClose();    //关闭等待提示
	    if(data.result == '1'){
			for(var i=0,l=data.list.length;i<l;i++){
				var json = data.list[i];
				
				//避免某个模版出问题导致整个模版显示不出来，所以用try包含
				try{
					var itemContent = template.replace(/{imgsrc}/g,"/plugin/kefu/template/"+json.name+"/preview.jpg");
					itemContent = itemContent.replace(/{templateName}/g,json.name);
					itemContent = itemContent.replace(/{info}/g,'贡献者:'+json.author);
					if(json.name == '${kefu.templateName}'){
						//当前使用的便是这个模版
						itemContent = itemContent.replace(/{useClass}/g,"layui-btn-disabled");
						itemContent = itemContent.replace(/{shiyongbutton}/g,"当前已使用");
					}else{
						itemContent = itemContent.replace(/{shiyongbutton}/g,"立即使用");
					}
					 
					html = html + itemContent;
				}catch(e){
					console.log(e);
				}
				
			}
			document.getElementById('templateList').innerHTML = html;
		}else if(data.result == '0'){
	         parent.iw.msgFailure(data.info);
		}else{
	         parent.iw.msgFailure();
		}
	});
}
loadTemplate();

//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
parent.layer.iframeAuto(index);

//更改模版
function updateTemplate(value){
	parent.iw.loading('更换中...');
	$.post("saveKefu.do?name=templateName&value="+value, function(data){
	    parent.iw.loadClose();    //关闭等待提示
	    if(data.result == '1'){
	        parent.iw.msgSuccess('更换成功');
	        parent.loadIframeByUrl('../../plugin/kefu/index.do');
	     }else if(data.result == '0'){
	         parent.iw.msgFailure(data.info);
	     }else{
	         parent.iw.msgFailure();
	     }
	});
}

</script>
<jsp:include page="../../iw/common/foot.jsp"></jsp:include>  