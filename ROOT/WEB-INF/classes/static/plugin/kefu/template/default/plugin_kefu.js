//加载样式文件
plugin_kefu_loadCss("http://res.weiunity.com/plugin/kefu/template/default/css/style.css");
//加载jquery.js
plugin_kefu_loadJs("http://res.weiunity.com/js/jquery-2.1.4.js", function(){
	//加载完Jquery之后，要执行的函数
	
	//加载 html 填充上客服的html代码
	var html = '<div class="plugin_kefu_bar"><ul><li class="plugin_kefu_top">返回顶部</li>';
	if(plugin_kefu_getParam('phone').length > 0){
		html = html + '<li class="plugin_kefu_phone">'+plugin_kefu_getParam('phone')+'</li>';
	}
	if(plugin_kefu_getParam('qq').length > 0){
		html = html + '<li class="plugin_kefu_QQ"><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin='+plugin_kefu_getParam('qq')+'&site=您的网站&menu=yes">QQ在线咨询</a></li>';
	}
	if(plugin_kefu_getParam('weixinImageUrl').length > 0){
		html = html + '<li class="plugin_kefu_ercode" style="height:53px;">微信扫一扫<br><img class="plugin_kefu_hd_qr" src="'+plugin_kefu_getParam('weixinImageUrl')+'" alt="微信扫一扫"></li>';
	}
	html = html + '</ul></div>';
	document.getElementById('plugin_kefu').innerHTML = html;
	
	
	// 悬浮窗口
	$("#plugin_kefu").hover(function() {
		$("#plugin_kefu").css("right", "5px");
		$(".plugin_kefu_bar .plugin_kefu_ercode").css('height', '200px');
	}, function() {
		$("#plugin_kefu").css("right", "-127px");
		$(".plugin_kefu_bar .plugin_kefu_ercode").css('height', '53px');
	});
	// 返回顶部
	$(".plugin_kefu_top").click(function() {
		$("html,body").animate({
			'scrollTop': '0px'
		}, 300)
	});
});
