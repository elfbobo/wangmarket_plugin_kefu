//加载样式文件
plugin_kefu_loadCss("http://res.weiunity.com/plugin/kefu/template/left1/css/style.css");
//加载jquery.js
plugin_kefu_loadJs("http://res.weiunity.com/js/jquery-2.1.4.js", function(){
	//加载完Jquery之后，要执行的函数
	
	//加载 html 填充上客服的html代码
	var html = '<ul class="plugin_kefu_icon"><li class="plugin_kefu_up" title="上一页"></li>';
	if(plugin_kefu_getParam('qq').length > 0){
		html = html + '<li class="plugin_kefu_qq"></li>';
	}
	if(plugin_kefu_getParam('phone').length > 0){
		html = html + '<li class="plugin_kefu_tel"></li>';
	}
	if(plugin_kefu_getParam('weixinImageUrl').length > 0){
		html = html + '<li class="plugin_kefu_wx"></li>';
	}
	html = html + '<li class="plugin_kefu_down" title="下一页"></li></ul>';
	
	html = html + '<ul class="plugin_kefu_info">'+
				'<li class="plugin_kefu_qq"><p>在线沟通，请点我<a href="http://wpa.qq.com/msgrd?v=3&uin='+plugin_kefu_getParam('qq')+'&site=qq&menu=yes" target="_blank">在线咨询</a></p></li>'+
				'<li class="plugin_kefu_tel"><p>咨询热线：<br>'+plugin_kefu_getParam('phone');
	if(plugin_kefu_getParam('qq').length > 0){
		html = html + '<br>客服qq：<br>'+plugin_kefu_getParam('qq')+'</p></li>';
	}
	html = html + '<li class="plugin_kefu_wx"><div class="plugin_kefu_img"><img src="'+plugin_kefu_getParam('weixinImageUrl')+'" width="115" height="115" /></div></li>';
	html = html + '</ul></div><div id="plugin_kefu_btn" class="plugin_kefu_index_cy"></div>';

	document.getElementById('plugin_kefu').innerHTML = html;
	
	//更改背景颜色
	//判断是否有设置背景颜色
	var plugin_kefu_backgroundColor = "018D75";	//默认颜色
	if(plugin_kefu_getParam('backgroundColor').length > 0){
		plugin_kefu_backgroundColor = plugin_kefu_getParam('backgroundColor');
	}
	document.getElementById('plugin_kefu').style.backgroundColor="#"+plugin_kefu_backgroundColor;
	$(".plugin_kefu_info").css({"background-color":"#"+plugin_kefu_backgroundColor});

	
	$('#plugin_kefu .plugin_kefu_icon li').not('.plugin_kefu_up,.plugin_kefu_down').mouseenter(function(){
		$('#plugin_kefu .plugin_kefu_info').addClass('hover');
		$('#plugin_kefu .plugin_kefu_info li').hide();
		$('#plugin_kefu .plugin_kefu_info li.'+$(this).attr('class')).show();//#plugin_kefu .plugin_kefu_info li.plugin_kefu_qq
	});
	$('#plugin_kefu').mouseleave(function(){
		$('#plugin_kefu .plugin_kefu_info').removeClass('hover');
	});
	$('#plugin_kefu_btn').click(function(){
		$('#plugin_kefu').toggle();
		if($(this).hasClass('index_cy')){
			$(this).removeClass('index_cy');
			$(this).addClass('index_cy2');
		}else{
			$(this).removeClass('index_cy2');
			$(this).addClass('index_cy');
		}
		
	});
		
});
