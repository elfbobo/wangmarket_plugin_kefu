/*
 * 网站管理后台-功能插件-在线客服中，可设置的配置项，配置哪些设置项显示、隐藏
 * true：显示配置项
 * false：隐藏配置项
 * 
 */
var plugin_kefu_config = new Array();
plugin_kefu_config['phone'] = true;
plugin_kefu_config['qq'] = true;
plugin_kefu_config['address'] = false;
plugin_kefu_config['weixinImageUrl'] = false;
plugin_kefu_config['backgroundColor'] = true;