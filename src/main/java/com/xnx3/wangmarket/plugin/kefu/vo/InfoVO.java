package com.xnx3.wangmarket.plugin.kefu.vo;

import com.xnx3.j2ee.vo.BaseVO;

/**
 * info.properties 文件的信息
 * @author 管雷鸣
 *
 */
public class InfoVO extends BaseVO{
	private String name; 	//模版名字，取的是文件夹名字
	private String info;	//简介
	private String author;	//作者
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	
}
