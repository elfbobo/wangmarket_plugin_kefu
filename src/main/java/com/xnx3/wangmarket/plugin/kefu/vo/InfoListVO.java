package com.xnx3.wangmarket.plugin.kefu.vo;

import java.util.ArrayList;
import java.util.List;

import com.xnx3.j2ee.vo.BaseVO;

/**
 * info.properties 列表，吧所有插件的信息列出来
 * @author 管雷鸣
 *
 */
public class InfoListVO extends BaseVO{
	private List<InfoVO> list;	//所有在线客服模版列表

	public List<InfoVO> getList() {
		if(list == null){
			list = new ArrayList<InfoVO>();
		}
		return list;
	}

	public void setList(List<InfoVO> list) {
		this.list = list;
	}
	
}
