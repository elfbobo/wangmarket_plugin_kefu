package com.xnx3.wangmarket.plugin.kefu.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Properties 工具类
 * @author 管雷鸣
 *
 */
public class PropertiesUtil {
	private Properties properties;
	
	public PropertiesUtil(String path) {
		try {
			read(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void read(String path) throws IOException{
		properties = new Properties();
		
		InputStream inputStream = new FileInputStream(new File(path));
		properties.load(inputStream);
		inputStream.close();
	}
	

	/**
	 * 获取 application.properties 的配置属性
	 * @param key 要获取的配置的名字，如 database.name
	 * @return 获取的配置的值。如果不存在，则返回 null
	 */
    public String getProperty(String key){
    	if(properties == null){
    		return null;
    	}
    	return properties.getProperty(key);
    }
    
    public static void main(String[] args) {
    	PropertiesUtil p = new PropertiesUtil("/Users/apple/git/wangmarket/target/classes/static/plugin/kefu/template/dibu1/info.properties");
    	System.out.println(p.getProperty("name"));
	}
}
