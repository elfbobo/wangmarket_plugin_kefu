package com.xnx3.wangmarket.plugin.kefu.controller;

import java.io.File;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;
import com.xnx3.wangmarket.plugin.kefu.util.PropertiesUtil;
import com.xnx3.wangmarket.plugin.kefu.vo.InfoListVO;
import com.xnx3.wangmarket.plugin.kefu.vo.InfoVO;
import com.xnx3.wangmarket.plugin.kefu_domain.entity.Kefu;
import com.xnx3.wangmarket.plugin.kefu_domain.bean.MQBean;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.j2ee.vo.UploadFileVO;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.util.AliyunLog;
import com.xnx3.wangmarket.domain.mq.DomainMQ;

/**
 * 在线客服插件
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/kefu/")
public class KefuPluginController extends BasePluginController{
	@Resource
	private SqlService sqlService;
	
	/**
	 * 后台首页
	 */
	@RequestMapping("index${url.suffix}")
	public String index(Model model){
		if(!haveSiteAuth()){
			return error(model, "无权使用");
		}
		
		Site site = getSite();
		Kefu kefu = sqlService.findById(Kefu.class, site.getId());
		if(kefu == null){
			// 如果用户未设置，第一次用，那么初始化,创建一个
			kefu = new Kefu();
			kefu.setSiteid(site.getId());
			kefu.setIsUse(Kefu.ISUSE_FALSE);	//默认不启用
			kefu.setTemplateName("default"); 	//默认使用默认的模版
			sqlService.save(kefu);
			
			sendMQ(site, kefu);
			generateCacheJS(kefu);
		}
		
		AliyunLog.addActionLog(getSiteId(), "进入客服插件首页");
		
		model.addAttribute("kefu", kefu);
		model.addAttribute("site", site);
		return "plugin/kefu/index";
	}
	

	/**
	 * 选择模版，更换在线客服模版
	 */
	@RequestMapping("selectTemplate${url.suffix}")
	public String selectTemplate(Model model){
		if(!haveSiteAuth()){
			return error(model, "无权使用");
		}
		
		Site site = getSite();
		Kefu kefu = sqlService.findById(Kefu.class, site.getId());
		
		
		AliyunLog.addActionLog(getSiteId(), "选择模版-客服插件");
		
		model.addAttribute("kefu", kefu);
		model.addAttribute("site", site);
		return "plugin/kefu/selectTemplate";
	}
	
	

	/**
	 * 更改当前网站，是否使用在线客服功能
	 * @param use 是否使用，1使用，0不使用
	 */
	@RequestMapping(value="useKefu${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO useKefu(@RequestParam(value = "use", required = false , defaultValue="0") short use){
		if(!haveSiteAuth()){
			return error("无权使用");
		}
		
		Site site = getSite();
		Kefu kefu = sqlService.findById(Kefu.class, site.getId());
		kefu.setIsUse(use - Kefu.ISUSE_TRUE == 0? Kefu.ISUSE_TRUE:Kefu.ISUSE_FALSE);
		sqlService.save(kefu);
		
		sendMQ(site, kefu);
		//生成js缓存
		generateCacheJS(kefu);
		//记录日志
		AliyunLog.addActionLog(kefu.getSiteid(), (use - Kefu.ISUSE_TRUE == 0 ? "开启":"关闭")+"插件的在线客服");
		
		return success();
	}
	

	/**
	 * 保存 kefu 相关信息
	 * @param name kefu 的列名，如 qq 、 phone等
	 * @param value 修改后的值
	 */
	@RequestMapping("saveKefu${url.suffix}")
	@ResponseBody
	public BaseVO saveKefu(HttpServletRequest request,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "value", required = true) String value){
		if(!haveSiteAuth()){
			return error("无权使用");
		}
		
		Site site = getSite();
		Kefu kefu = sqlService.findById(Kefu.class, site.getId());
		value = filter(value);
		
		switch (name) {
		case "address":
			kefu.setAddress(value);
			AliyunLog.addActionLog(kefu.getSiteid(), "插件-在线客服，更改联系地址", kefu.getAddress());	//记录操作日志
			break;
		case "phone":
			kefu.setPhone(value);
			AliyunLog.addActionLog(kefu.getSiteid(), "插件-在线客服，更改联系电话", kefu.getPhone());	//记录操作日志
			break;
		case "templateName":
			kefu.setTemplateName(value);
			AliyunLog.addActionLog(kefu.getSiteid(), "插件-在线客服，更改模版", kefu.getTemplateName());	//记录操作日志
			break;
		case "qq":
			kefu.setQq(value);
			AliyunLog.addActionLog(kefu.getSiteid(), "插件-在线客服，更改模版", kefu.getTemplateName());	//记录操作日志
			break;	
		case "weixinImageUrl":
			kefu.setWeixinImageUrl(value);
			AliyunLog.addActionLog(kefu.getSiteid(), "插件-在线客服，更改微信二维码", kefu.getWeixinImageUrl());	//记录操作日志
			break;	
		case "backgroundColor":
			kefu.setBackgroundColor(value);
			AliyunLog.addActionLog(kefu.getSiteid(), "插件-在线客服，更改背景颜色", kefu.getBackgroundColor());	//记录操作日志
			break;	
		default:
			return error("name无效");
		}
		
		sqlService.save(kefu);
		sendMQ(site, kefu);
		//更新缓存
		generateCacheJS(kefu);
		return success();
	}
	

	/**
	 * 上传微信二维码图片接口
	 */
	@RequestMapping(value="uploadWeixinImageUrl${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public UploadFileVO uploadWeixinImageUrl(Model model,HttpServletRequest request){
		UploadFileVO uploadFileVO = new UploadFileVO();
		if(!haveSiteAuth()){
			uploadFileVO.setBaseVO(UploadFileVO.FAILURE, "请先登录");
			return uploadFileVO;
		}
		Site site = getSite();
		
		uploadFileVO = AttachmentFile.uploadImage("site/"+site.getId()+"/plugin/kefu/", request, "image", 0);
		if(uploadFileVO.getResult() == UploadFileVO.SUCCESS){
			Kefu kefu = sqlService.findById(Kefu.class, site.getId());
			kefu.setWeixinImageUrl(uploadFileVO.getUrl());
			sqlService.save(kefu);
			
			//更新缓存
			generateCacheJS(kefu);
			sendMQ(site, kefu);
			//上传成功，写日志
			AliyunLog.addActionLog(site.getId(), "插件-在线客服上传微信二维码："+uploadFileVO.getUrl());
		}
		
		return uploadFileVO;
	}
	
	
	/**
	 * 创建 /site/siteid/data/plugin_kefu.js
	 * @param kefu {@link Kefu}对象
	 */
	private void generateCacheJS(Kefu kefu){
		StringBuffer sb = new StringBuffer();
		sb.append("var plugin_kefu = new Array(); ");
		sb.append("plugin_kefu['isUse'] = "+kefu.getIsUse()+";");
		sb.append("plugin_kefu['siteid'] = "+kefu.getSiteid()+";");
		sb.append("plugin_kefu['address'] = '"+kefu.getAddress()+"';");
		sb.append("plugin_kefu['phone'] = '"+kefu.getPhone()+"';");
		sb.append("plugin_kefu['qq'] = '"+kefu.getQq()+"';");
		sb.append("plugin_kefu['templateName'] = '"+kefu.getTemplateName()+"';");
		sb.append("plugin_kefu['weixinImageUrl'] = '"+kefu.getWeixinImageUrl()+"';");
		sb.append("plugin_kefu['backgroundColor'] = '"+kefu.getBackgroundColor()+"';");
		
		AttachmentFile.putStringFile("site/"+kefu.getSiteid()+"/data/plugin_kefu.js", sb.toString());
	}
	/**
	 * 向 domain 项目发送mq更新消息
	 * @param site 当前登陆的站点
	 * @param kefu 当前最新的 {@link Kefu} 对象
	 */
	private void sendMQ(Site site, Kefu kefu){
		MQBean mq = new MQBean();
		mq.setKefu(kefu);
		mq.setSiteid(site.getId());
		mq.setDomain(site.getDomain());
		mq.setBindDomain(site.getBindDomain());
		DomainMQ.send("kefu", mq.toJsonString());
	}
	
	/**
	 * 获取项目static静态资源的plugin文件夹路径。
	 * 如 /Library/Tomcat8/tomcat8/webapps/ROOT/static/plugin/
	 * @return
	 */
	private String getPluginStaticPath(){
		String projectPath = Global.getProjectPath();
		if(projectPath.indexOf("/target/classes/") > -1){
			//是在Eclipse开发环境
			return projectPath+"static/plugin/kefu/template/";
		}else{
			//是在正式环境
			return projectPath+"WEB-INF/classes/static/plugin/kefu/template/";
		}
	}
	/**
	 * 可选模版列表
	 */
	@RequestMapping("templateList${url.suffix}")
	@ResponseBody
	public InfoListVO templateList(Model model){
		//AliyunLog.addActionLog(getSiteId(), "客服模版列表查看");
		InfoListVO listVO = new InfoListVO();
		
		//遍历模版文件夹下，有多少个模版文件
		///Library/Tomcat8/tomcat8/webapps/ROOT/static/plugin/kefu/template/
		///Users/apple/git/wangmarket/target/classes/static/plugin/kefu/template/
		File file = new File(getPluginStaticPath());
		if(!file.exists()){
			listVO.setBaseVO(InfoListVO.FAILURE, "出错，未发现 static/plugin/kefu/template/ 文件夹");
			return listVO;
		}
		
		//模版文件列表
		File[] templateFileList = file.listFiles();
		for (int i = 0; i < templateFileList.length; i++) {
			File templateFile = templateFileList[i];
			if(!templateFile.isDirectory()){
				//如果不是目录，那么忽略这个文件
				continue;
			}
			File infoFile = new File(templateFile.getPath()+"/info.properties");
			if(!infoFile.exists()){
				//如果没有 info.properties 文件，那么认为这个文件夹不是个正常的模板文件，忽略这个文件夹
				continue;
			}
			PropertiesUtil propertiesUtil = new PropertiesUtil(infoFile.getAbsolutePath());
			
			InfoVO infoVO = new InfoVO();
			infoVO.setName(templateFile.getName());
			infoVO.setAuthor(propertiesUtil.getProperty("author"));
			infoVO.setInfo(propertiesUtil.getProperty("info"));
			listVO.getList().add(infoVO);
		}
		
		
		return listVO;
	}
	
}
