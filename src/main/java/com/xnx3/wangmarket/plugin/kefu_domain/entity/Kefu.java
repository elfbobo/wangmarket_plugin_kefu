package com.xnx3.wangmarket.plugin.kefu_domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 网站的im在线客服设置
 */
@Entity
@Table(name = "plugin_kefu")
public class Kefu implements java.io.Serializable {

	/**
	 * 是否启用，true：1  启用
	 */
	public static final Short ISUSE_TRUE = 1;
	/**
	 * 是否启用，false：0  不启用
	 */
	public static final Short ISUSE_FALSE = 0;
	
	
	private Integer siteid;		//对应网站的id，主键
	private Short isUse;	//是否启用，当前网站是否使用在线客服功能，默认为0，不启用。  1为启用
	private String templateName;	//使用的在线客服的模版，在网站上显示时是用的哪个模版显示，这里是模版名字，也就是相当于模版的id，模版唯一识别标志。如果为空，默认使用 default 模版
	private String qq;	//联系QQ
	private String weixinImageUrl;	//微信二维码的图片url绝对路径
	private String phone;	//联系电话
	private String address;	//办公地址
	private String backgroundColor;	//在线客服的背景颜色，十六进制，如 FFFFFF，若不设置，则为空字符串
	
	public Kefu() {
		this.templateName = "";
	}
	
	@Id
	@Column(name = "siteid", unique = true, nullable = false)
	public Integer getSiteid() {
		return this.siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}
	
	@Column(name = "template_name", columnDefinition="char(30) comment '使用的在线客服的模版，在网站上显示时是用的哪个模版显示，这里是模版名字，也就是相当于模版的id，模版唯一识别标志。如果为空，默认使用 default 模版'")
	public String getTemplateName() {
		if(templateName == null){
			return "";
		}
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	@Column(name = "qq", columnDefinition="char(50) comment '联系QQ'")
	public String getQq() {
		if(qq == null){
			return "";
		}
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}
	
	@Column(name = "weixin_image_url", columnDefinition="char(255) comment '微信二维码的图片url绝对路径'")
	public String getWeixinImageUrl() {
		if(weixinImageUrl == null){
			return "";
		}
		return weixinImageUrl;
	}

	public void setWeixinImageUrl(String weixinImageUrl) {
		this.weixinImageUrl = weixinImageUrl;
	}
	
	@Column(name = "phone", columnDefinition="char(50) comment '联系电话'")
	public String getPhone() {
		if(phone == null){
			return "";
		}
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Column(name = "address", columnDefinition="char(100) comment '办公地址'")
	public String getAddress() {
		if(address == null){
			return "";
		}
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "is_use", columnDefinition="tinyint(2) comment '是否启用，当前网站是否使用在线客服功能，默认为0，不启用。  1为启用'")
	public Short getIsUse() {
		return isUse;
	}

	public void setIsUse(Short isUse) {
		this.isUse = isUse;
	}
	
	@Column(name = "background_color", columnDefinition="char(6) comment '在线客服的背景颜色，十六进制，如 FFFFFF，若不设置，则为空字符串'")
	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

}