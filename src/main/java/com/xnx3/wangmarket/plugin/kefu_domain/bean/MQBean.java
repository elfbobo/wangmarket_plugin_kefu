package com.xnx3.wangmarket.plugin.kefu_domain.bean;

import com.xnx3.wangmarket.plugin.kefu_domain.entity.Kefu;

import net.sf.json.JSONObject;

/**
 * MQ 传递的消息
 * @author 管雷鸣
 *
 */
public class MQBean {
	private int siteid;	//站点id
	private String domain;	//站点自动分配的二级域名
	private String bindDomain;	//站点绑定的域名
	private Kefu kefu;	//传递的主要的消息体，Kefu
	
	public MQBean() {
	}
	
	/**
	 * 创建 MQBean对象，并吧 MQ 传递来的消息体转化为 MQBean对象
	 * @param mqContent MQ传递来的消息体
	 */
	public MQBean(String mqContent) {
		JSONObject json = JSONObject.fromObject(mqContent);
		siteid = json.getInt("siteid");
		domain = json.getString("domain");
		if(json.get("bindDomain") != null){
			bindDomain = json.getString("bindDomain");
		}
		
		JSONObject kefuJson = json.getJSONObject("kefu");
		kefu = new Kefu();
		kefu.setIsUse((short)kefuJson.getInt("isUse"));
		kefu.setAddress(kefuJson.getString("address"));
		kefu.setPhone(kefuJson.getString("phone"));
		kefu.setQq(kefuJson.getString("qq"));
		kefu.setTemplateName(kefuJson.getString("templateName"));
		kefu.setWeixinImageUrl(kefuJson.getString("weixinImageUrl"));
		kefu.setBackgroundColor(kefuJson.getString("backgroundColor"));
		
		System.out.println(this.toString());
	}
	
	/**
	 * 将本MQ对象转化为 string 格式的 json字符串
	 * @return string 格式的，以便 mq 传递消息
	 */
	public String toJsonString(){
		JSONObject json = JSONObject.fromObject(this);
		System.out.println("---"+json.toString());
		return json.toString();
	}
	
	public int getSiteid() {
		return siteid;
	}
	public void setSiteid(int siteid) {
		this.siteid = siteid;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getBindDomain() {
		return bindDomain;
	}
	public void setBindDomain(String bindDomain) {
		this.bindDomain = bindDomain;
	}
	public Kefu getKefu() {
		return kefu;
	}
	public void setKefu(Kefu kefu) {
		this.kefu = kefu;
	}
	
	@Override
	public String toString() {
		return "MQBean [siteid=" + siteid + ", domain=" + domain + ", bindDomain=" + bindDomain + ", kefu=" + kefu
				+ "]";
	}
	
}
