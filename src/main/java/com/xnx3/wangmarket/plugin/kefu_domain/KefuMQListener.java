package com.xnx3.wangmarket.plugin.kefu_domain;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;
import com.xnx3.j2ee.func.Log;
import com.xnx3.wangmarket.domain.G;
import com.xnx3.wangmarket.domain.bean.SimpleSite;
import com.xnx3.wangmarket.domain.mq.DomainMQ;
import com.xnx3.wangmarket.domain.mq.ReceiveDomainMQ;
import com.xnx3.wangmarket.plugin.kefu_domain.bean.MQBean;

/**
 * MQ消息监听，接收网站管理后台设置的参数
 * @author 管雷鸣
 *
 */
@Component
public class KefuMQListener {
	public KefuMQListener() {
		Log.info("domain load kefu plugin");
		DomainMQ.receive("kefu", new ReceiveDomainMQ() {
			public void receive(String content) {
				MQBean mq = new MQBean(content);
				if(mq.getBindDomain() != null && mq.getBindDomain().length() > 0){
					//有自己绑定的域名，那么更新绑定域名的缓存
					SimpleSite ss = G.bindDomainSiteMap.get(mq.getBindDomain());
					if(ss == null){
						//这种情况理论上是不存在的
						Log.error("-----error---"+mq);
						return;
					}
					Map<String,Object> map = ss.getPlugin().get("kefu");
					if(map == null){
						map = new HashMap<String,Object>();
					}
					map.put("isUse", mq.getKefu().getIsUse());
					map.put("templateName", mq.getKefu().getTemplateName().length() == 0? "default":mq.getKefu().getTemplateName());
					map.put("backgroundColor", mq.getKefu().getBackgroundColor());
					
					ss.getPlugin().put("kefu", map);
					G.bindDomainSiteMap.put(mq.getBindDomain(), ss);
				}
				
				if(mq.getDomain() != null && mq.getDomain().length() > 0){
					//有分配的二级域名，那么更新绑定域名的缓存。当然，二级域名只要是创建网站，肯定都是会分配的
					SimpleSite ss = G.domainSiteMap.get(mq.getDomain());
					Map<String,Object> map = ss.getPlugin().get("kefu");
					if(map == null){
						map = new HashMap<String,Object>();
					}
					map.put("isUse", mq.getKefu().getIsUse());
					map.put("templateName", mq.getKefu().getTemplateName().length() == 0? "default":mq.getKefu().getTemplateName());
					map.put("backgroundColor", mq.getKefu().getBackgroundColor());
					
					ss.getPlugin().put("kefu", map);
					G.domainSiteMap.put(mq.getDomain(), ss);
				}
			}
		});
		
	}
	
}
