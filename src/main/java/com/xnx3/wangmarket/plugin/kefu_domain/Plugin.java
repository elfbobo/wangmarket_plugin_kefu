package com.xnx3.wangmarket.plugin.kefu_domain;

import java.util.Map;
import com.xnx3.DateUtil;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.wangmarket.domain.bean.RequestInfo;
import com.xnx3.wangmarket.domain.bean.SimpleSite;
import com.xnx3.wangmarket.domain.pluginManage.interfaces.DomainVisitInterface;

/**
 * domain 注册插件
 * @author 管雷鸣
 *
 */
public class Plugin implements DomainVisitInterface{

	public String htmlManage(String html, SimpleSite simpleSite, RequestInfo requestInfo) {
		Map<String,Object> kefuMap = simpleSite.getPlugin().get("kefu");
		if(kefuMap == null){
			//为空，则是没有使用到该插件，将html 原样返回
			return html;
		}
		if(kefuMap.get("isUse").toString().equals("1")){
			//如果当前网站启用了插件-在线客服功能，那么就要向html末尾追加在线客服的js
			Object templateNameObject = kefuMap.get("templateName");
			String templateName = "default";
			if(templateNameObject != null){
				templateName = templateNameObject.toString();
			}
			
//			String kfText = "<div id=\"plugin_kefu\"></div>"
//					+ "<script src=\""+AttachmentFile.netUrl()+"site/"+simpleSite.getId()+"/data/plugin_kefu.js?time="+DateUtil.timeForUnix10()+"\"></script>"	//加载设置的数据
//					+ "<script src=\"http://res.weiunity.com/plugin/kefu/js/plugin_kefu_function.js\"></script>"
//					+ "<script src=\"http://res.weiunity.com/plugin/kefu/template/"+templateName+"/plugin_kefu.js\"></script>";
			String kfText = "<div id=\"plugin_kefu\"></div>"
					+ "<script src=\""+AttachmentFile.netUrl()+"site/"+simpleSite.getId()+"/data/plugin_kefu.js?time="+DateUtil.timeForUnix10()+"\"></script>"	//加载设置的数据
					+ "<script src=\""+Global.get("MASTER_SITE_URL")+"plugin/kefu/js/plugin_kefu_function.js\"></script>"
					+ "<script src=\""+Global.get("MASTER_SITE_URL")+"plugin/kefu/template/"+templateName+"/plugin_kefu.js\"></script>";
			
			html = html + kfText;
		}
		
		return html;
	}
}
