package com.xnx3.wangmarket.plugin.kefu_domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;
import com.xnx3.Lang;
import com.xnx3.wangmarket.domain.G;
import com.xnx3.wangmarket.domain.bean.SimpleSite;
import com.xnx3.j2ee.func.Log;

/**
 * 项目启动初始化，从数据库加载qq客服是否使用的数据到缓存
 * @author 管雷鸣
 */
@Component
public class InitLoadDataByDB {
	public static boolean cache = false;	//是否已缓存，记录。若为true，则是已缓存
	
	public InitLoadDataByDB() {
		new Thread(new Runnable() {
			
			public void run() {
				boolean b = true;
				while(b){
					try {
						//延迟 10 秒
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(G.getDomainSize() > 0){
						b = false;
					}else{
						//项目中的二级域名个数是0，还没有加载域名，等待加载完域名数据后，在进行加载这里的数据
					}
				}
				
				loadData();
			}
		}).start();
		
	}
	
	/**
	 * 加载数据，从数据库中
	 */
	public void loadData(){
		if(cache){
			//已缓存，无需再缓存了
			return;
		}
		cache = true;
		
		List<Map<String, Object>> list = com.xnx3.j2ee.func.Sql.getSqlService().findMapBySqlQuery("SELECT plugin_kefu.siteid, plugin_kefu.is_use, plugin_kefu.template_name FROM plugin_kefu");
		Log.info("plugin_kefu load "+list.size()+" number data");
		//将数据库查询到的结果，以 key ： siteid，  value ：查询到的结果 ， 的方式，存入map，以便根据siteid来取信息
		Map<Integer, Map<String, Object>> kefuMap = new HashMap<Integer, Map<String, Object>>();
		for (int i = 0; i < list.size(); i++) {
			Map<String, Object> map = list.get(i);
			if(map.get("siteid") != null){
				map.put("isUse", map.get("is_use"));
				map.put("templateName", map.get("template_name"));
				map.remove("is_use");
				map.remove("template_name");
				
				kefuMap.put(Lang.stringToInt(map.get("siteid").toString(), 0), map);
			}
		}
		
		//遍历域名绑定，将加载到的信息，更新到用户绑定域名的缓存中
		for (Map.Entry<String, SimpleSite> entry : G.bindDomainSiteMap.entrySet()) {
			SimpleSite ss = entry.getValue();
			Map<String, Object> setMap = kefuMap.get(ss.getId());	//这个站点设置的在线客服相关功能
			if(setMap != null){
				//这个站点有在线客服的配置，加载
//				ss.getPlugin().put("kefu", setMap);
				Map<String, Map<String, Object>> pluginMap = ss.getPlugin();
				
				pluginMap.put("kefu", setMap);
				ss.setPlugin(pluginMap);
				G.bindDomainSiteMap.put(ss.getBindDomain(), ss);
			}
        }

		//遍历域名绑定，将加载到的信息，更新到用户二级域名的缓存中
		for (Map.Entry<String, SimpleSite> entry : G.domainSiteMap.entrySet()) {
			SimpleSite ss = entry.getValue();
			Map<String, Object> setMap = kefuMap.get(ss.getId());	//这个站点设置的在线客服相关功能
			if(setMap != null){
				//这个站点有在线客服的配置，加载
//				Map<String, Object> ssMap =  ss.getPlugin().get("kefu");
				Map<String, Map<String, Object>> pluginMap = ss.getPlugin();
				
				pluginMap.put("kefu", setMap);
				ss.setPlugin(pluginMap);
				G.domainSiteMap.put(ss.getDomain(), ss);
			}
        }

	}
	
}
