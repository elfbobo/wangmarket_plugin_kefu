/template 模版相关
如果用户没有选择模版，则这个便是默认的模版

这三个是固定的，每个模版必须有的
config.js 网站管理后台-功能插件-在线客服中，可设置的配置项，配置哪些设置项显示、隐藏
plugin_kefu.js 网站前端显示，网站访问时显示
demo.html 演示效果的页面
preview.jpg 效果图	800*1000，背景白色
info.properties 这个模版的一些属性信息，注意是utf8编码。包含
		author 作者名字
		info 模版简介，一两句话简单说明模版
		可以使用 com.xnx3.StringUtil.StringToUtf8("文字") 将文字转为 utf8编码
css/	这三个都是模版的资源文件
js/
images/