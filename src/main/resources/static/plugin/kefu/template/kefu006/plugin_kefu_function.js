/**
 * 加载JS文件，加载完后执行指定的函数
 * jsUrl 要加载的JS文件的URL地址，绝对路径
 * exec 加载完JS后要执行的方法，传入 function(){}
 */
function plugin_kefu_loadJs(jsUrl, exec){
	var _doc=document.getElementsByTagName('head')[0];  
	var script=document.createElement('script');  
	script.setAttribute('type','text/javascript');  
	script.setAttribute('src',jsUrl);  
	_doc.appendChild(script);  
	script.onload=script.onreadystatechange=function(){  
		if(!this.readyState||this.readyState=='loaded'||this.readyState=='complete'){  
			exec();
		}
		script.onload=script.onreadystatechange=null;  
	}
}

/**
 * 加载 css 文件
 * path 加载的css文件的绝对路径url
 */
function plugin_kefu_loadCss(path){
	var link = document.createElement('link');
	link.href = path;
	link.rel = 'stylesheet';
	link.type = 'text/css';
	document.getElementsByTagName('head')[0].appendChild(link);
}

/**
 * 获取 js 的缓存信息 plugin_kefu_getParam
 */
function plugin_kefu_getParam(name){
	if(typeof(plugin_kefu) == 'undefined'){
		return '';
	}
	if(typeof(plugin_kefu[name]) == 'undefined'){
		return '';
	}
	if(plugin_kefu[name] == 'null'){
		return '';
	}
	return plugin_kefu[name];
}

