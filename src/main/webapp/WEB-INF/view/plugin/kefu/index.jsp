<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.shiro.ShiroFunc"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../iw/common/head.jsp">
	<jsp:param name="title" value="在线客服设置"/>
</jsp:include>
<script src="/plugin/kefu/template/${kefu.templateName}/config.js"></script>

<div id="shifouqiyong" class="layui-form" style="padding-top:16%; text-align:center;">
	<span id="qiyongtishi">是否启用在线客服功能</span> &nbsp;&nbsp;&nbsp;
	<input type="checkbox" id="switchInputId" name="use" value="1" lay-filter="useKefu" lay-skin="switch" lay-text="开启|关闭" <c:if test="${kefu.isUse == 1}">checked</c:if>>
</div>
<div id="setPanel" style="padding-top:50px;">
	<table style="width:100%;">
		<tr>
			<td style="width:40%">
				<div style="padding-left:20%; width:60%; text-align:center;">
					<img src="/plugin/kefu/template/${kefu.templateName}/preview.jpg" style="height:200px; max-width:100%;" />
					<div style="padding-top:40px;">
						<a class="layui-btn" href="selectTemplate.do">更换模版</a>
					</div>
				</div>
			</td>
			<td style="width:50%; padding-right:10%;">
				
				<table class="layui-table layui-form" lay-even lay-skin="nob" style="margin:0px; padding:0px;">
					<tbody>
					
					
						<tr class="kefuSetInfo qq_tr">
							<td id="qq_td">QQ号</td>
							<td onclick="popUpdate('qq','客服QQ', '${kefu.qq}');" style="cursor: pointer;">
								${kefu.qq}
								<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
							</td>
						</tr>
						<tr class="kefuSetInfo weixinImageUrl_tr" >
							<td id="head_td">微信二维码</td>
							<td>
								<a id="headAId" href="${kefu.weixinImageUrl }" title="点击预览原图" target="_black"><img id="kefuHead" src="${kefu.weixinImageUrl }?x-oss-process=image/resize,h_25" height="25" /></a>
								<button type="button" class="layui-btn layui-btn-primary layui-btn-sm" id="uploadKefuHeadButton" style="margin-left:20px;">
									<i class="layui-icon">&#xe67c;</i>上传图片
								</button>
							</td>
						</tr>
						<tr class="kefuSetInfo phone_tr">
							<td id="autoReply_td">客服电话</td>
							<td onclick="popUpdate('phone','客服电话', '${kefu.phone}');" style="cursor: pointer;">
								${kefu.phone}
								<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
							</td>
						</tr>
						<tr class="kefuSetInfo address_tr">
							<td id="autoReply_td">办公地址</td>
							<td onclick="popUpdate('address','办公地址', '${kefu.address}');" style="cursor: pointer;">
								${kefu.address}
								<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
							</td>
						</tr>
						<tr class="kefuSetInfo backgroundColor_tr">
							<td id="autoReply_td">背景颜色</td>
							<!-- onclick="popUpdate('backgroundColor','背景颜色，如 FFFFFF', '${kefu.backgroundColor}');" -->
							<td  style="cursor: pointer;">
								<span id="backgroundColorValue">${kefu.backgroundColor}</span>
								<i class="layui-icon" style="padding-left:8px; font-size:21px;" id="backgroundColor_i">&#xe642;</i> 
							</td>
						</tr>
						<tr class="kefuSetInfo" style="display:;">
							<td id="autoReply_td">模版</td>
							<td onclick="popUpdate('templateName','模版', '${kefu.templateName}');" style="cursor: pointer;">
								${kefu.templateName}
								<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
							</td>
						</tr>
						
					</tbody>
				</table>
				
			</td>
		</tr>
	</table>
	
	
	<div style="color:#a2a2a2; text-align:left; padding-top:30px; padding-bottom: 10px; padding-left:20px;">
		提示：<br/>
		1. 请设置上面要设置的项，设置上之后才会显示。比如，你设置了QQ号，网站中的在线客服，才会出现QQ交谈这一项，若是不设置，则不显示这项<br/>
		2. 有的网站使用某个客服模版后，网站上会出现错乱，如果这样，就请选择一个其他的客服模版。这里的每个客服模版，并不是适用于所有的网站，有可能会被网站本身的代码所影响。您可以选择一个适合自己网站的在线客服模版。
	</div>

	
</div>




<script>




/**
 * 获取 js 的缓存信息 plugin_kefu_getParam
 */
function plugin_kefu_getParam(name){
	if(typeof(plugin_kefu_config) == 'undefined'){
		return '';
	}
	if(typeof(plugin_kefu_config[name]) == 'undefined'){
		return '';
	}
	if(plugin_kefu_config[name] == 'null'){
		return '';
	}
	return plugin_kefu_config[name];
}

//根据选择的客服模版不同，加载不同的配置项。下面便是判断配置项是否显示
try{
	$(".kefuSetInfo").css("display","none");
	if(plugin_kefu_getParam('phone')){
		$(".phone_tr").css("display","");
	}
	if(plugin_kefu_getParam('address')){
		$(".address_tr").css("display","");
	}
	if(plugin_kefu_getParam('qq')){
		$(".qq_tr").css("display","");
	}
	if(plugin_kefu_getParam('backgroundColor')){
		$(".backgroundColor_tr").css("display","");
	}
	if(plugin_kefu_getParam('weixinImageUrl')){
		$(".weixinImageUrl_tr").css("display","");
	}
}catch(e){}
</script>


<script type="text/javascript">
//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
parent.layer.iframeAuto(index);

layui.use('form', function(){
	var form = layui.form;
	
	form.on('switch(useKefu)', function(data){
		useKefuChange(data.elem.checked);
		updateUseKefu(data.elem.checked? '1':'0');	//将改动同步到服务器，进行保存
	});
	
	//美化是否启用的开关控件
	$(".layui-form-switch").css("marginTop","-2px");
});

layui.use('upload', function(){
	var upload = layui.upload;
	upload.render({
    	elem: '#uploadKefuHeadButton' //绑定元素
		,url: 'uploadWeixinImageUrl.do' //上传接口
		,field: 'image'
		,before: function(obj){
			parent.iw.loading('上传中...');
		}
		,done: function(res){
			parent.iw.loadClose();
			parent.iw.msgSuccess('上传成功');
			document.getElementById('kefuHead').src = res.url+'?x-oss-process=image/resize,h_25';
			document.getElementById('headAId').href  = res.url;
		}
		,error: function(){
			//请求异常回调
			parent.iw.loadClose();
			parent.iw.msgFailure('上传出错');
		}
	});
});

//是否使用客服的开关发生改变触发  use  true:开启使用状态
function useKefuChange(use){
	if(use){
		//使用
		//$(".kefuSetInfo").css("opacity","1.0");
		document.getElementById('shifouqiyong').style.paddingTop = '20px';
		document.getElementById('qiyongtishi').innerHTML = '点右侧可关掉在线客服功能 -->>';
		document.getElementById('setPanel').style.display = '';
		
	}else{
		//不使用
		//$(".kefuSetInfo").css("opacity","0.3");
		document.getElementById('shifouqiyong').style.paddingTop = '16%';
		document.getElementById('qiyongtishi').innerHTML = '要想在您的网站中使用在线客服功能，请点右侧启用 -->>';
		
		document.getElementById('setPanel').style.display = 'none';
	}
}
useKefuChange('${kefu.isUse}' == 1);

//修改当前客服是否使用
function updateUseKefu(value){
	parent.iw.loading('修改中...');
	$.post("/plugin/kefu/useKefu.do?use="+value, function(data){
	    parent.iw.loadClose();    //关闭“操作中”的等待提示
	    if(data.result == '1'){
	        parent.iw.msgSuccess('操作成功');
	     }else if(data.result == '0'){
	         parent.iw.msgFailure(data.info);
	     }else{
	         parent.iw.msgFailure();
	     }
	});
}

/**
 * 弹出修改窗口，修改 agency 的某个信息
 * @param name kefu的数据表某列名字，如qq、address
 * @param description 描述，弹出窗口的说明信息
 * @param oldValue 旧的值，当前的值
 */
function popUpdate(name, description, oldValue){
	layer.prompt({
	  formType: 2,
	  value: oldValue,
	  title: description,
	  area: ['400px', '100px'] //自定义文本域宽高
	}, function(value, index, elem){
	  layer.close(index);
	  parent.iw.loading('修改中');
 	 $.post(
	    "saveKefu.do", 
	    { "name": name, "value":value }, 
	    function(data){
			console.log(data);
	        parent.iw.loadClose();    //关闭“更改中”的等待提示
	        if(data.result != '1'){
	            parent.iw.msgFailure(data.info);
	        }else{
	            parent.iw.msgSuccess("操作成功");
	            location.reload();
	        }
	    }, 
	"json");
	});
}


//鼠标跟随提示
$(function(){
	//是否启用在线客服
	var td_useKefu_index = 0;
	$("#useKefu_td").hover(function(){
		td_useKefu_index = layer.tips('您是否想在您的网站中启用在线客服呢？<br/>启用在线客服后，会在网站的所有页面都会出现在线客服功能。', '#useKefu_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(td_useKefu_index);
	})
	
	
	//微信二维码
	var head_td_index = 0;
	$("#head_td").hover(function(){
		head_td_index = layer.tips('在线客服中，显示的 微信扫一扫 的二维码。请使用正方形的图片！', '#head_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(head_td_index);
	})
	
});
</script>

<script>


//颜色选择器
layui.use('colorpicker', function(){
  var colorpicker = layui.colorpicker;
  colorpicker.render({
    elem: '#backgroundColor_i'
    ,color: '#'+document.getElementById('backgroundColorValue').innerHTML
    ,done: function(color){
    	var c = document.getElementById('backgroundColorValue').innerHTML = color.substring(1);
    	
    	$.post(
	    "saveKefu.do", 
	    { "name": "backgroundColor", "value":c }, 
	    function(data){
	        parent.iw.loadClose();    //关闭“更改中”的等待提示
	        if(data.result != '1'){
	            parent.iw.msgFailure(data.info);
	        }else{
	            parent.iw.msgSuccess("操作成功");
	            location.reload();
	        }
	    }, 
	"json");
    }
  });
  
});
</script>

<jsp:include page="../../iw/common/foot.jsp"></jsp:include>  