# 在线客服
[网市场云建站系统插件](https://gitee.com/mail_osc/wangmarket) 之 在线客服插件，快速让网站拥有在线客服功能。

## 使用说明
1. 功能使用入口在 网站管理后台 - 功能插件 - 在线客服
2. 开启在线客服后，查看网站，即可看到网站已经增加了在线客服功能

## 使用条件
注意，本项目不能直接运行，需要放到 [网市场云建站系统](https://gitee.com/mail_osc/wangmarket) 中才可运行使用，且网市场云建站系统本身需要 v4.5 或以上版本才可。
只支持Mysql数据库

## 直接使用
1. 下载本项目
1. 将 /ROOT 目录下的文件，复制出来，粘贴到你的运行项目中，也就是 tomcat/webapps/ROOT/ 下
1. 如果你是分布式部署，重复上面步骤，将ROOT目录下的文件放到你的domain域名解析项目的 tomcat/webapps/ROOT/ 下,同时要删除掉这个目录 ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/kefu/ 
1. 重新启动运行项目，登陆网站管理后台，即可看到左侧的 功能插件 下多了名为 在线客服 的功能插件。
1. 点开 在线客服 的功能插件，即可开始使用。

## 二次开发
1. 首先导入主项目，也就是我们网市场云建站系统 https://gitee.com/mail_osc/wangmarket 将此项目导入，能运行起来
1. 下载本项目，将 /src/ 目录下的文件，按照目录结构，粘贴入 网市场云建站系统(wangmarket) 的主项目中。
1. 再次运行项目，登陆网站管理后台（默认运行起来后，自带的网站管理后台默认账号密码都是 wangzhan ，可以用此账号登陆）
1. 登陆网站管理后台后，找到左侧的 功能插件 下的 在线客服 即可。
1. 上述步骤运行起来后，便可开始进行本插件功能的二次开发了

## 文件及目录结构
#### 前端展示页面 (springmvc 的 view)
可根据你自己喜好，更改模版的前端展示样式。当前，前端页面都在 src/main/webapp/WEB-INF/view/plugin/kefu/ 目录下

````
├── index.jsp	网站管理后台中，客服插件的首页，也就是设置页面，比如设置QQ、联系电话、以及客服是否启用
└── selectTemplate.jsp	选择客服模版
````

#### 后端 (springmvc 的 Controller)
###### com.xnx3.wangmarket.plugin.kefu 管理后台
````
├── controller	controller控制器，具体方法函数实现
|   └── KefuPluginController.java	网站管理后台中的相关客服设置，如是否开启客服功能，设置QQ、电话等
└── Plugin.java	插件注册类，将本插件注册进网市场云建站系统中
````

###### com.xnx3.wangmarket.plugin.kefu_domain 域名解析
````
├── bean	bean类
|   └── MQBean	MQ传递的消息，发送、接收都会使用到
├── entity	实体类
|   └── Kefu	对应数据表 plugin_kefu
├── InitLoadDataByDB.java	项目启动后自动初始化，从数据库加载qq客服是否使用的数据，到缓存中存储
├── KefuMQListener.java	MQ消息监听，接收mq发送过来的消息
└── Plugin.java	插件注册类，将本插件注册进网市场云建站系统中的 domain(域名解析)项目中，也就是 com.xnx3.wangmarket.domain 包
````

## 功能截图

![](//cdn.weiunity.com/site/341/news/6cfc6b1988bc4d7d824de349d20a4447.png)
![](//cdn.weiunity.com/site/341/news/eaa82fe000cf4f6cacd18b1a237b2663.jpg)

## 交流及求助
交流社区： [bbs.wang.market](http://bbs.leimingyun.com) 求助请到社区论坛发帖，有专人负责解答疑惑<br/>
QQ群：472328584

## 合作洽谈
作者：管雷鸣<br/>
微信：xnx3com<br/>
QQ：921153866<br/>

## 在线客服模版开发说明
模版文件都是放在这个路径：
ROOT/WEB-INF/classes/static/plugin/kefu/template/<br/>
比如，你可以复制 default 这个文件夹，再起个别的名字。这就是又增加了一套模版


## 插件开发说明
|||
| ------------- |-------------|
| 管理后台插件的开发及示例 | [iw.xnx3.com/5902.html](http://iw.xnx3.com/5902.html) |
| 网站访问插件的开发及示例 | [iw.xnx3.com/5818.html](http://iw.xnx3.com/5818.html) |